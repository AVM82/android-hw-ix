package org.rabilint.homework.service

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.rabilint.homework.BuildConfig
import org.rabilint.homework.repository.network.NetworkRepository
import org.rabilint.homework.utilities.URL_API
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object NetworkService {

    private var interceptor = HttpLoggingInterceptor().apply {
        level =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY
            else HttpLoggingInterceptor.Level.NONE
    }

    private val okHttpClient = OkHttpClient.Builder()
        .readTimeout(30, TimeUnit.SECONDS)
        .connectTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .addInterceptor(interceptor)
        .build()

    fun create(): NetworkRepository {
        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(URL_API)
            .client(okHttpClient)
            .build()

        return retrofit.create(NetworkRepository::class.java)
    }

}