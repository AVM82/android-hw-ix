package org.rabilint.homework.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import org.rabilint.homework.model.Post
import org.rabilint.homework.repository.database.dao.DatabaseDao
import org.rabilint.homework.service.NetworkService
import timber.log.Timber

class MainViewModel(private val dataSource: DatabaseDao) : ViewModel() {

    private var job = Job()
    private var scope = CoroutineScope(Dispatchers.Main + job)

    private val networkService by lazy {
        NetworkService.create()
    }

    private var _postList = MutableLiveData<List<Post>>()
    val postList: LiveData<List<Post>>
        get() = _postList

    private val _navigateToPostDetail = MutableLiveData<Long>()
    val navigateToPostDetail: LiveData<Long>
        get() = _navigateToPostDetail

    private fun onFetchPost() {
        Timber.d("Fetch Post")
        scope.launch {
            _postList.value = getPostsFromDb()
        }
    }

    private suspend fun getPostsFromDb(): List<Post>? {
        return withContext(Dispatchers.IO) {
            dataSource.getAllPosts()
        }
    }

    fun onPostClicked(id: Long) {
        _navigateToPostDetail.value = id
    }

    fun onPostDetailNavigated() {
        _navigateToPostDetail.value = null
    }

    var getAllPosts = {
        networkService.getAllPosts()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response -> _postList.value = response },
                { error ->
                    Timber.e(error, "ERROR getAllPosts")
                    onFetchPost()
                }
            )
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }
}
