package org.rabilint.homework.ui.main.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.rabilint.homework.databinding.PostItemViewBinding
import org.rabilint.homework.model.Post


class PostListAdapter(private val clickListener: PostClockListener) :
    ListAdapter<Post, PostListAdapter.ViewHolderPostList>(
        PostDiffCallback()
    ) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderPostList {
        return ViewHolderPostList.from(
            parent
        )
    }

    override fun onBindViewHolder(holderPostList: ViewHolderPostList, position: Int) {
        val item = getItem(position)
        holderPostList.bind(clickListener, item)
    }


    class ViewHolderPostList private constructor(private val binding: PostItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bind(clickListener: PostClockListener, item: Post) {
            binding.post = item
            binding.executePendingBindings()
            binding.clickListener = clickListener
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolderPostList {
                val inflater = LayoutInflater.from(parent.context)
                val binding = PostItemViewBinding.inflate(inflater, parent, false)
                return ViewHolderPostList(
                    binding
                )
            }
        }

    }

}

class PostDiffCallback : DiffUtil.ItemCallback<Post>() {

    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem == newItem
    }

}

class PostClockListener(val clickListener: (postId: Long) -> Unit) {
    fun onClick(post: Post) {
        clickListener(post.id)
    }
}

