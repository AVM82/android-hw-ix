package org.rabilint.homework.ui.postDetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import org.rabilint.homework.model.Comment
import org.rabilint.homework.model.Post
import org.rabilint.homework.model.User
import org.rabilint.homework.repository.database.dao.DatabaseDao
import org.rabilint.homework.service.NetworkService
import timber.log.Timber

class PostDetailViewModel(private val dataSource: DatabaseDao) : ViewModel() {

    private var job = Job()
    private var scope = CoroutineScope(Dispatchers.Main + job)

    private val networkService by lazy {
        NetworkService.create()
    }

    private val _postId = MutableLiveData<Long>()
    val postId: LiveData<Long>
        get() = _postId

    private val _user = MutableLiveData<User>()
    val user: LiveData<User>
        get() = _user

    private val _post = MutableLiveData<Post>()
    val post: LiveData<Post>
        get() = _post

    private val _comments = MutableLiveData<List<Comment>>()
    val comments: LiveData<List<Comment>>
        get() = _comments

    fun onPostDetailNavigated(id: Long) {
        _postId.value = id
    }


    private fun onFetchPostById(id: Long) {
        scope.launch {
            _post.value = withContext(Dispatchers.IO) {
                dataSource.getPostById(id)
            }
            _post.value?.userId?.let { onFetchUserById(it) }
        }
    }

    private fun onFetchUserById(id: Long) {
        scope.launch {
            _user.value = withContext(Dispatchers.IO) {
                dataSource.getUserById(id)
            }
        }
    }

    private fun onFetchCommentByPostId(id: Long) {
        scope.launch {
            _comments.value = withContext(Dispatchers.IO) {
                dataSource.getCommentByPostId(id)
            }
        }
    }

    private fun onSavePost(post: Post) {
        scope.launch {
            insertPost(post)
        }
    }

    private fun onSaveUser(user: User) {
        Timber.d("user-> %s", user.toString())
        scope.launch {
            insertUser(user)
        }
    }

    private fun onSaveComment(comment: List<Comment>) {
        scope.launch {
            insertComment(comment)
        }
    }

    private suspend fun insertPost(post: Post) {
        withContext(Dispatchers.IO) {
            dataSource.insertPost(post)
        }
    }

    private suspend fun insertUser(user: User) {
        withContext(Dispatchers.IO) {
            dataSource.insertUser(user)
        }
    }

    private suspend fun insertComment(comment: List<Comment>) {
        withContext(Dispatchers.IO) {
            dataSource.insertComment(comment)
        }
    }

    private fun fetchPost(postId: Long?) {
        postId?.let {
            networkService.getPostById(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { response ->
                        _post.value = response
                        onSavePost(response)
                        fetchUserById(response.userId)
                    },
                    { error ->
                        Timber.e(error, "ERROR fetchPost in PostDetail")
                        onFetchPostById(postId)
                    }
                )
        }
    }

    private fun fetchComments(postId: Long?) {
        postId?.let {
            networkService.getCommentsByPostId(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { response ->
                        _comments.value = response
                        onSaveComment(response)
                    },
                    { error ->
                        Timber.e(error, "fetchComments ERROR")
                        onFetchCommentByPostId(postId)
                    }
                )
        }
    }

    private fun fetchUserById(userId: Long?) {
        userId?.let {
            networkService.getUserById(it)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { response ->
                        _user.value = response
                        onSaveUser(response)
                    },
                    { error ->
                        Timber.e(error, "ERROR fetchUserById")
                        onFetchUserById(userId)
                    }
                )
        }
    }

    val loadData = {
        apply {
            fetchPost(_postId.value)
            fetchComments(_postId.value)
        }
    }

    override fun onCleared() {
        super.onCleared()
        job.cancel()
    }


}