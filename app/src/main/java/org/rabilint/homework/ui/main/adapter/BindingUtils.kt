package org.rabilint.homework.ui.main.adapter

import android.widget.TextView
import androidx.databinding.BindingAdapter
import org.rabilint.homework.model.Post

@BindingAdapter("postTitle")
fun TextView.setSleepDurationFormatted(item: Post?) {
    item?.let {
        text = item.title
    }
}

@BindingAdapter("postID")
fun TextView.setPostId(item: Post?) {
    item?.let {
        val postId = "Post id: ${item.id}"
        text = postId
    }
}
