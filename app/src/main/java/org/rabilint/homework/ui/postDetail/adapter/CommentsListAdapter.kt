package org.rabilint.homework.ui.postDetail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import org.rabilint.homework.databinding.CommentItemViewBinding
import org.rabilint.homework.model.Comment

class CommentsListAdapter : ListAdapter<Comment, CommentsListAdapter.ViewHolderCommentList>(
    CommentDiffCallback()
) {

    override fun onBindViewHolder(holder: ViewHolderCommentList, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderCommentList {
        return ViewHolderCommentList.from(parent)
    }

    class ViewHolderCommentList private constructor(private val binding: CommentItemViewBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bind(item: Comment) {
            binding.commentAuthor.text = item.name
            binding.commentText.text = item.body
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolderCommentList {
                val inflater = LayoutInflater.from(parent.context)
                val binding = CommentItemViewBinding.inflate(inflater, parent, false)
                return ViewHolderCommentList(
                    binding
                )
            }
        }
    }
}

class CommentDiffCallback : DiffUtil.ItemCallback<Comment>() {

    override fun areItemsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Comment, newItem: Comment): Boolean {
        return oldItem == newItem
    }
}