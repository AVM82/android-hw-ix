package org.rabilint.homework.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import org.rabilint.homework.R
import org.rabilint.homework.databinding.MainFragmentBinding
import org.rabilint.homework.repository.database.PostDatabase
import org.rabilint.homework.ui.main.adapter.PostClockListener
import org.rabilint.homework.ui.main.adapter.PostListAdapter
import timber.log.Timber

class MainFragment : Fragment() {

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.main_fragment,
            container,
            false
        )
        Timber.d("[Called ViewModelProvider for MainViewModel]")
        val application = requireNotNull(this.activity).application
        val dataSource = PostDatabase.getInstance(application).databaseDao

        val viewModelFactory = MainViewModelFactory(dataSource)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(MainViewModel::class.java)


        binding.mainViewModel = viewModel

        viewModel.navigateToPostDetail.observe(viewLifecycleOwner, Observer { postId ->
            postId?.let {
                val bundle = bundleOf("postId" to postId)
                this.findNavController()
                    .navigate(R.id.action_main_fragment_to_post_detail_fragment, bundle)
                viewModel.onPostDetailNavigated()
            }
        })

        val adapter = PostListAdapter(
            PostClockListener { postId ->
                viewModel.onPostClicked(postId)
            })

        binding.postList.adapter = adapter
        viewModel.postList.observe(viewLifecycleOwner, Observer {
            it?.let { adapter.submitList(it) }
        })

        binding.lifecycleOwner = this
        return binding.root

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.getAllPosts()
    }

}