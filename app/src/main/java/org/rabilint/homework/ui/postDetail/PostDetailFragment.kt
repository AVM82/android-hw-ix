package org.rabilint.homework.ui.postDetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import org.rabilint.homework.R
import org.rabilint.homework.databinding.PostDetailFragmentBinding
import org.rabilint.homework.repository.database.PostDatabase
import org.rabilint.homework.ui.postDetail.adapter.CommentsListAdapter
import timber.log.Timber

class PostDetailFragment : Fragment() {

    private lateinit var postDetailViewModel: PostDetailViewModel
    private lateinit var binding: PostDetailFragmentBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.post_detail_fragment,
            container,
            false
        )

        Timber.d("[Called0ViewModelProvider for PostDetailViewModel]")
        postDetailViewModel = makeViewModal()
        binding.postDetailViewModal = postDetailViewModel
        binding.lifecycleOwner = this

        val adapter = CommentsListAdapter()
        binding.commentList.adapter = adapter
        postDetailViewModel.comments.observe(viewLifecycleOwner, Observer {
            it?.let { adapter.submitList(it) }
        })

        postDetailViewModel.postId.observe(viewLifecycleOwner, Observer {
            it?.let { postDetailViewModel.loadData() }
        })

        return binding.root
    }

    private fun makeViewModal(): PostDetailViewModel {
        val application = requireNotNull(this.activity).application
        val dataSource = PostDatabase.getInstance(application).databaseDao

        val viewModelFactory = PostDetailViewModelFactory(dataSource)

        return ViewModelProvider(this, viewModelFactory).get(PostDetailViewModel::class.java)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        postDetailViewModel.onPostDetailNavigated(arguments?.get("postId") as Long)
    }

}