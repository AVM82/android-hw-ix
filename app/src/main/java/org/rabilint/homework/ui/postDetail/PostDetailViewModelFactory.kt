package org.rabilint.homework.ui.postDetail

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import org.rabilint.homework.repository.database.dao.DatabaseDao

class PostDetailViewModelFactory(
    private val dataSource: DatabaseDao
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostDetailViewModel::class.java)) {
            return PostDetailViewModel(dataSource) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}