package org.rabilint.homework.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comment")
data class Comment(
    @PrimaryKey
    val id: Long = 0,
    val postId: Long = 0,
    val name: String = "",
    val email: String = "",
    val body: String = ""
)