package org.rabilint.homework.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "post")
data class Post(
    @PrimaryKey
    val id: Long = 0,

    @ColumnInfo(name = "user_id")
    val userId: Long = 0,

    val title: String = "",

    val body: String = ""
)
