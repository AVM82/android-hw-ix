package org.rabilint.homework.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @PrimaryKey
    val id: Long = 0,
    val name: String = "",
    val username: String = "",
    val email: String = "",
    val website: String = ""
)