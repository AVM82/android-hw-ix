package org.rabilint.homework.utilities

const val URL_API = "http://jsonplaceholder.typicode.com"
const val GET_POSTS = "/posts"
const val GET_USERS = "/users"
const val GET_COMMENTS_BY_POST_ID = "/posts/{id}/comments"