package org.rabilint.homework.repository.network

import io.reactivex.Observable
import org.rabilint.homework.model.Comment
import org.rabilint.homework.model.Post
import org.rabilint.homework.model.User
import org.rabilint.homework.utilities.GET_COMMENTS_BY_POST_ID
import org.rabilint.homework.utilities.GET_POSTS
import org.rabilint.homework.utilities.GET_USERS
import retrofit2.http.GET
import retrofit2.http.Path

interface NetworkRepository {

    @GET(GET_POSTS)
    fun getAllPosts(): Observable<List<Post>>

    @GET("${GET_USERS}/{Id}")
    fun getUserById(@Path("Id") Id: Long): Observable<User>

    @GET(GET_USERS)
    fun getUsers(): Observable<List<User>>

    @GET("${GET_POSTS}/{Id}")
    fun getPostById(@Path("Id") Id: Long): Observable<Post>

    @GET(GET_COMMENTS_BY_POST_ID)
    fun getCommentsByPostId(@Path("id") Id: Long): Observable<List<Comment>>

}