package org.rabilint.homework.repository.database.dao

import androidx.room.*
import org.rabilint.homework.model.Comment
import org.rabilint.homework.model.Post
import org.rabilint.homework.model.User

@Dao
interface DatabaseDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPost(post: Post)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertComment(comment: List<Comment>)

    @Update
    fun updatePost(post: Post)

    @Query("SELECT * FROM post")
    fun getAllPosts(): List<Post>

    @Query("SELECT * FROM user WHERE id = :id")
    fun getUserById(id: Long): User?

    @Query("SELECT * FROM comment WHERE postId = :id")
    fun getCommentByPostId(id: Long): List<Comment>

    @Query("SELECT * FROM post WHERE id = :id")
    fun getPostById(id: Long): Post?

    @Query("DELETE FROM post")
    fun clear()


}