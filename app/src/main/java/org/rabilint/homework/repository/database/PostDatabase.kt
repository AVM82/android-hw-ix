package org.rabilint.homework.repository.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import org.rabilint.homework.model.Comment
import org.rabilint.homework.model.Post
import org.rabilint.homework.model.User
import org.rabilint.homework.repository.database.dao.DatabaseDao

@Database(entities = [Post::class, User::class, Comment::class], version = 1)
abstract class PostDatabase : RoomDatabase() {
    abstract val databaseDao: DatabaseDao

    companion object {
        @Volatile
        private var INSTANCE: PostDatabase? = null

        fun getInstance(context: Context): PostDatabase {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        PostDatabase::class.java,
                        "post_database"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                }
                return instance
            }
        }
    }
}
